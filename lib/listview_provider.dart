
import 'dart:math';

import 'package:flutter/cupertino.dart';

class ListViewProvider with ChangeNotifier {

  List<int> _listOfInts = [];

  List<int> get listOfInts {
    return _listOfInts;
  }

  bool asc = true;


  void generateList () {
    _listOfInts = [...List.generate(15, (index) => index)];
    notifyListeners();
  }

  void sortList() {
    if (!asc) {
      _listOfInts.sort((a,b) => a.compareTo(b));
      asc = true;
    } else {
      _listOfInts.sort((b,a) => a.compareTo(b));
      asc = false;
    }
    notifyListeners();
  }

  Random random =  Random();
  
  void removeRandom() {
    _listOfInts.removeAt(random.nextInt(_listOfInts.length));
    notifyListeners();
  }
}