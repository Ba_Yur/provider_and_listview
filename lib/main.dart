import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'listview_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ListViewProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ListViewProvider provider = Provider.of<ListViewProvider>(context);
    return Scaffold(
        body: Column(
      children: [
        Expanded(
          child: ListView.builder(
            itemCount: provider.listOfInts.length > 0
                ? provider.listOfInts.length
                : 0,
            itemBuilder: (context, index) {
              return Text(
                provider.listOfInts[index].toString(),
                style: TextStyle(
                  fontSize: 25,
                ),
                textAlign: TextAlign.center,
              );
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
              onPressed: () {
                provider.generateList();
              },
              child: Text('Generate'),
            ),
            ElevatedButton(
              onPressed: () {
                provider.sortList();
              },
              child: Text('Sort'),
            ),
            ElevatedButton(
              onPressed: () {
                provider.removeRandom();
              },
              child: Text('Remove random item'),
            ),
          ],
        ),
      ],
    ));
  }
}
